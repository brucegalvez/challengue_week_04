from phoneBook import PhoneBook
from menu import Menu

# Llamamos al menu interactivo
menu = Menu()

while True:
    optionMenu = menu.mostrar()
    
    print(f'Estamos recibiendo la {optionMenu}')

    phoneBook = PhoneBook()

    phoneBook.convNumAFunc(optionMenu)

