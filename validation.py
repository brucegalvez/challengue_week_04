def validarDNI():
    correcto=False
    
    while(not correcto):
        try:
            dni = input("Introduce un numero DNI que no sea mayor a 8 digitos: ")
            if len(dni) != 8:
                print('error')
            else:
                return dni

        except ValueError:
            print('Error, introduce un numero entero')
     
    return dni

dni = validarDNI()
print(dni)