from contacto import Contacto

class PhoneBook:

    contactos = []

    def añadirContacto(self):
        contacto = Contacto(
            nombre = input("Ingrese un nombre: "),
            apellido = input("Ingrese un apellido: "),
            telefono = input("Ingrese un numero de telefono: "),
            email = input("Ingrese un email: "),
            dni = input("Ingrese un DNI: "))
        self.contactos.append(contacto)
        print("Contacto agregado con exito!")
        
    def enlistarContactos(self,listaContactos = contactos):
        for contacto in listaContactos:
            print(vars(contacto))

    def buscarContacto(self):
        contactos = self.contactos
        attrABuscar = input("Ingrese el atributo a buscar (dni/nombre/apellido): ")
        if(attrABuscar in ('dni','nombre','apellido')):
            busqueda = input("Ingrese el valor a buscar: ")
            indexValidos = []
            contactosValidos = []
            for i in range(len(contactos)):
                if(getattr(contactos[i],attrABuscar)==busqueda):
                    indexValidos.append(i)
                    contactosValidos.append(contactos[i])
            if not indexValidos:
                print("No existe contacto con esos datos")
            else:
                print("Resultado de la busqueda:")
                self.enlistarContactos(contactosValidos)
            return indexValidos
        else:
            print("Opcion invalida")
    
    def definirContacto(self, index):
        cantContactos = len(index)
        indexSelec = 0
        if(cantContactos>1):
            #Capturar errores al ingresar el valor
            indexSelec = int(input('Seleccione con un numero un contacto de la lista (1-{}): '.format(cantContactos)))-1
        else:
            indexSelec = index[0]
        contactoSelec = self.contactos[indexSelec]
        print('Se ha seleccionado el contacto {}'.format(vars(contactoSelec)))
        return indexSelec

    def editarContacto(self):
        indexFilt = self.buscarContacto()
        if not indexFilt:
            return
        index = self.definirContacto(indexFilt)
        atributos = self.contactos[index].__dict__.keys()
        atributosFormat = ''
        for atributo in atributos:
            atributosFormat = atributosFormat+'/'+atributo
        attrAEditar = input('Ingrese el atributo a editar ({}): '.format(atributosFormat))
        if(attrAEditar not in (atributos)):
            print('No existe ese atributo')
        else:
            nuevoValor = input('Ingrese el nuevo valor: ')
            setattr(self.contactos[index],attrAEditar,nuevoValor)
            print('Se ha modificado el "{}" a "{}" con exito!'.format(attrAEditar,nuevoValor))
            
    def eliminarContacto(self):
        indexFilt = self.buscarContacto()
        if not indexFilt:
            return
        index = self.definirContacto(indexFilt)
        while True:
            confirmacion = input("Seguro que deseas eliminar este contacto? (si/no): ")
            if(confirmacion=="no"):
                break
            elif(confirmacion=="si"):
                del self.contactos[index]
                print("Contacto eliminado con exito!")
                break

    def convNumAFunc(self, argument):
        map = {
            1 : self.añadirContacto,
            2 : self.enlistarContactos,
            3 : self.buscarContacto,
            4 : self.editarContacto,
            5 : self.eliminarContacto}
        func = map.get(argument, lambda: "Opcion invalida")
        func()

    
